#pragma once
#include <SFML/Graphics.hpp>
#include <vector>


class Bullet
{
public:
	//Constructor
	Bullet(sf::Texture& bulletTexture, sf::Vector2u newscreenbounds, sf::Vector2f startpos, sf::Vector2f newVelocity);
	void update(sf::Time frameTime);
	void draw(sf::RenderWindow& gameWindow);
	bool getalive();
	sf::FloatRect getHit();
	void SetAlive(bool newAlive);
	sf::Sprite Sprite;
private:
	
	sf::Vector2f Velocity;
	sf::Vector2u screenBounds;
	bool Alive;
};

