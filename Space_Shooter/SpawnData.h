#pragma once
#include <SFML/Graphics.hpp>


struct spawnData
{
	sf::Vector2f position;
	std::vector< sf::Vector2f > pattern;
	sf::Time Delay;
};