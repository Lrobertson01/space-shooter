#include <string>
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <vector>

#include <cstdlib>
#include <time.h>
#include "Player.h"
#include "Stars.h"
#include "Bullet.h"
#include "Enemy.h"
#include "SpawnData.h"

int main()
{

    // -----------------------------------------------
    // Game Setup
    // -----------------------------------------------
    sf::Font gameFont;
    gameFont.loadFromFile("Assets/Font/mainFont.ttf");

    sf::Music gameMusic;
    gameMusic.openFromFile("Assets/Audio/music.OGG");
    gameMusic.play();

    sf::SoundBuffer gameOverSoundEffect;
    gameOverSoundEffect.loadFromFile("Assets/Audio/Loss.OGG");
    sf::Sound gameOverSound;
    gameOverSound.setBuffer(gameOverSoundEffect);

    sf::SoundBuffer WinSoundEffect;
    WinSoundEffect.loadFromFile("Assets/Audio/win.OGG");
    sf::Sound WinSound;
    WinSound.setBuffer(WinSoundEffect);

    bool gameOver = false;
    bool win = false;

    srand(time(NULL));
    //Create the game window
    sf::RenderWindow gameWindow;
    gameWindow.create(sf::VideoMode::getDesktopMode(), "SpaceShooter", sf::Style::Titlebar | sf::Style::Close);
    //Bullets 
    std::vector<Bullet> PlayerBullets;
    std::vector<Bullet> EnemyBullets;
    sf::Texture playerBulletTexture;
    playerBulletTexture.loadFromFile("Assets/Graphics/playerBullet.png");
    sf::Texture EnemyBulletTexture;
    EnemyBulletTexture.loadFromFile("Assets/Graphics/enemyBullet.png");
    sf::SoundBuffer bulletSound;
    bulletSound.loadFromFile("Assets/Audio/fire.ogg");

    //Create the player 
    sf::Texture playerTexture;
    playerTexture.loadFromFile("Assets/Graphics/player.png");
    Player playerObject(playerTexture, gameWindow.getSize(), PlayerBullets, playerBulletTexture, bulletSound);
    sf::Time invincibilityTime = sf::seconds(0);
    sf::Time maxInvincTime = sf::seconds(2);

    //Create the background
    sf::Texture starTexture;
    starTexture.loadFromFile("Assets/Graphics/Star.png");
    std::vector<Stars> stars;
    int numStars = 5;
    for (int i = 0; i < numStars; ++i)
    {
        stars.push_back(Stars(starTexture, gameWindow.getSize()));
    }



    //score 
    int score = 0;
    sf::Text scoreText;
    scoreText.setFont(gameFont);

    scoreText.setString("Score: 0");
    scoreText.setCharacterSize(16);
    scoreText.setFillColor(sf::Color::White);
    scoreText.setPosition(gameWindow.getSize().x - scoreText.getLocalBounds().width - 20, 60);

    sf::Text livesText;
    livesText.setFont(gameFont);

    livesText.setString("lives: 0");
    livesText.setCharacterSize(16);
    livesText.setFillColor(sf::Color::White);
    livesText.setPosition(gameWindow.getSize().x - livesText.getLocalBounds().width - 20, 80);

    //text for game over
    sf::Text gameoverText;
    gameoverText.setFont(gameFont);

    gameoverText.setString("Press R to Restart");
    gameoverText.setCharacterSize(64);
    gameoverText.setFillColor(sf::Color::White);
    gameoverText.setPosition(gameWindow.getSize().x / 2, (gameWindow.getSize().y / 2) - gameoverText.getLocalBounds().height * 2);

    sf::Text EscapeText;
    EscapeText.setFont(gameFont);

    EscapeText.setString("Press escape to quit");
    EscapeText.setCharacterSize(24);
    EscapeText.setFillColor(sf::Color::White);
    EscapeText.setPosition(0,0);

    sf::Text WinText;
    WinText.setFont(gameFont);

    WinText.setString("YOU WIN!");
    WinText.setCharacterSize(64);
    WinText.setFillColor(sf::Color::White);
    WinText.setPosition(gameWindow.getSize().x / 2, (gameWindow.getSize().y / 2) + WinText.getLocalBounds().height * 2);


    //Movement Pattern
    std::vector< sf::Vector2f > MovePattern;
    int waves = 0;
    int spawnIndex = 0; //tells us what spawn we are currently on (hence the name)

    sf::Texture EnemyTexture;
    EnemyTexture.loadFromFile("Assets/Graphics/enemy.png");
    std::vector<Enemy> Enemies;

    //Create the enemy/spawn info
    std::vector<spawnData> spawnVector;
    sf::Time timeToSpawn;

    //Create Timer
    sf::Clock gameClock;

    // -----------------------------------------------
    // Game Running
    // -----------------------------------------------
    while (gameWindow.isOpen())
    {
        sf::Event event;
        while (gameWindow.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                gameWindow.close();
        }

        // -----------------------------------------------
        // Input
        // -----------------------------------------------
        playerObject.input(gameWindow.getSize());

        // -----------------------------------------------
        // Update
        // -----------------------------------------------


        sf::Time frameTime = gameClock.restart();
        playerObject.update(frameTime, gameWindow.getSize());
        // Update the stars
        scoreText.setString("Score: " + std::to_string((int)score));
        scoreText.setPosition(gameWindow.getSize().x - scoreText.getLocalBounds().width - 20, 60); //reset the scoretext position because otherwise it does weird things and looks off

        livesText.setString("lives: " + std::to_string((int)playerObject.getLives()));
        livesText.setPosition(gameWindow.getSize().x - livesText.getLocalBounds().width - 20, 80); //reset the livestext position because otherwise it does weird things and looks off

        for (int i = 0; i < stars.size(); ++i)
        {
            stars[i].update(frameTime);
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
        {
            //ability to quit the game at any time by pressing esc
            gameWindow.close();
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
        {
            //test for victory
            waves = 5;
        }

        if (!gameOver) 
        {

            if (playerObject.getLives() <= 0) 
            {
                gameMusic.stop();
                gameOverSound.play();
                gameOver = true;
                
            }

            if (waves == 5) 
            {
                gameMusic.stop();
                WinSound.play();
                win = true;
                gameOver = true;
            }

            //Set the wave system
            if (Enemies.size() <= 0 && waves < 5) //Known issue where if you snipe an enemy out of the gate it'll skip that wave
            {

                spawnVector.push_back({ sf::Vector2f(gameWindow.getSize().x,1000), MovePattern, sf::seconds(0.0f) });
                spawnVector.push_back({ sf::Vector2f(gameWindow.getSize().x,500), MovePattern, sf::seconds(0.0f) });
                spawnVector.push_back({ sf::Vector2f(gameWindow.getSize().x,250), MovePattern, sf::seconds(1.25f) });

                //spawn an extra enemy in each wave
                for (int i = 0; i < waves; ++i)
                {
                    spawnVector.push_back({ sf::Vector2f(gameWindow.getSize().x,250), MovePattern, sf::seconds(1.25f) });
                }
                timeToSpawn = spawnVector[spawnIndex].Delay;
                ++waves;
            }


            for (int i = 0; i < PlayerBullets.size(); ++i)
            {
                PlayerBullets[i].update(frameTime);
                //Check if the bullet is dead
                if (!PlayerBullets[i].getalive())
                {
                    // Remove the item from the vector
                    //Tried a breakpoint, its working
                    PlayerBullets.erase(PlayerBullets.begin() + i);
                }

            }


            //Enemies


            for (int i = 0; i < Enemies.size(); ++i)
            {
                Enemies[i].update(frameTime, gameWindow.getSize());
                if (!Enemies[i].getalive())
                {
                    // Remove the item from the vector
                    //Tried a breakpoint, its working
                    Enemies.erase(Enemies.begin() + i);
                }

            }




            for (int i = 0; i < EnemyBullets.size(); ++i)
            {
                EnemyBullets[i].update(frameTime);
                //Check if the bullet is dead
                if (!EnemyBullets[i].getalive())
                {
                    // Remove the item from the vector
                    //Tried a breakpoint, its working
                    EnemyBullets.erase(EnemyBullets.begin() + i);
                }

            }

            //Check if the bullet does the killing
            for (int i = 0; i < PlayerBullets.size(); ++i)
            {

                for (int j = Enemies.size() - 1; j >= 0; --j)
                {
                    sf::FloatRect bulletBounds = PlayerBullets[i].getHit();
                    sf::FloatRect enemyBounds = Enemies[j].getHitbox();
                    if (bulletBounds.intersects(enemyBounds))
                    {
                        //Delete the enemy
                        PlayerBullets[i].SetAlive(false);
                        Enemies[j].SetAlive(false);
                        score += 100;
                    }
                }
            }

            for (int i = 0; i < EnemyBullets.size(); ++i)
            {
                invincibilityTime -= frameTime;
                sf::FloatRect bulletBounds = EnemyBullets[i].getHit();
                sf::FloatRect playerBounds = playerObject.getHitbox();
                if (invincibilityTime <= sf::seconds(0) && bulletBounds.intersects(playerBounds))
                {
                    //Delete the enemy
                    EnemyBullets[i].SetAlive(false);
                    playerObject.setLives(1);
                    invincibilityTime = maxInvincTime;
                }
            }

            //check if it is time to spawn the enemy
            timeToSpawn -= frameTime; 
            if (timeToSpawn <= sf::seconds(0) && spawnIndex < spawnVector.size())
            {
                //Set a new move pattern before each spawn
                //The pattern is randomised using srand (which was declared up at the top), i set it to be % by the last position in the pattern so it only goes forward. 
                //The exception to this is Y, which i only set once because it would quickly become very hard to manage if we have several ships zipping up and down the screen
                MovePattern.push_back(sf::Vector2f(rand() % gameWindow.getSize().x + 10, (rand() % gameWindow.getSize().y + 10))); //+10 because otherwise it might choose 0
                MovePattern.push_back(sf::Vector2f(rand() % (int)MovePattern[0].x+ 10, (int)MovePattern[0].y + 10)); //if it chooses 0 it causes a crash
                MovePattern.push_back(sf::Vector2f(rand() % (int)MovePattern[1].x + 10, (int)MovePattern[1].y + 10)); // if you know a cleaner way to fix this please include it in the feedback
                MovePattern.push_back(sf::Vector2f(0, MovePattern[2].y));


                spawnVector[spawnIndex].pattern = MovePattern;

                Enemies.push_back(Enemy(EnemyTexture, gameWindow.getSize(), EnemyBullets, EnemyBulletTexture, bulletSound, spawnVector[spawnIndex].position, spawnVector[spawnIndex].pattern));

                ++spawnIndex;
                if (spawnIndex < spawnVector.size())
                {
                    timeToSpawn = spawnVector[spawnIndex].Delay;
                }
                //Clear the pattern, to be set again at the next spawn, to allow for each one to get a unique pattern
                MovePattern.clear();

            }
        }

        if (gameOver)
        {
            scoreText.setPosition(gameWindow.getSize().x / 2, gameWindow.getSize().y / 2);
            scoreText.setCharacterSize(64);

            if (sf::Keyboard::isKeyPressed(sf::Keyboard::R))
            {
                //ability to quit the game at any time by pressing esc
                gameOver = false;
                waves = 0; //Resets the waves system

                Enemies.clear();
                EnemyBullets.clear();
                PlayerBullets.clear();
                playerObject.reset(gameWindow.getSize(), playerTexture);
                spawnVector.clear();
                spawnIndex = 0;
                score = 0;
                
                gameMusic.play();
                timeToSpawn = sf::seconds(0);
                win = false;

                scoreText.setCharacterSize(16);
            }
        }
        // -----------------------------------------------
        // Drawing
        // -----------------------------------------------
        gameWindow.clear();
        for (int i = 0; i < stars.size(); ++i)
        {
            stars[i].draw(gameWindow);
        }

        if (gameOver) 
        {
            gameWindow.draw(gameoverText);
            if (win) 
            {
                gameWindow.draw(WinText);
            }
        }
        if (!gameOver) 
        {
            for (int i = 0; i < PlayerBullets.size(); ++i)
            {
                PlayerBullets[i].draw(gameWindow);
            }

            for (int i = 0; i < EnemyBullets.size(); ++i)
            {
                EnemyBullets[i].draw(gameWindow);
            }

            for (int i = 0; i < Enemies.size(); ++i)
            {
                Enemies[i].draw(gameWindow);
            }
            playerObject.draw(gameWindow);
        }
        gameWindow.draw(scoreText);
        gameWindow.draw(livesText);
        gameWindow.draw(EscapeText);

        gameWindow.display();
    }

    return 0;
}