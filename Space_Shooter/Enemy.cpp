#include "Enemy.h"
#include "SpawnData.h"



Enemy::Enemy(sf::Texture& Enemytexture, sf::Vector2u screensize, std::vector<Bullet>& newBullets, sf::Texture& newBulletTexture, sf::SoundBuffer& firingSoundBuffer,sf::Vector2f SpawnPos, std::vector< sf::Vector2f > NewMovePat) //Use an initialisation list
    :Velocity(0.0f, 0.0f)
    , Sprite(Enemytexture)
    , speed(300.0f)
    , bullets(&newBullets)
    , bulletTexture(&newBulletTexture)
    , bulletCooldownRemaining(sf::seconds(0.0f))
    , bulletCooldownMax(sf::seconds(1.0f))
    , bulletfireSound(firingSoundBuffer)
    , MovePat(NewMovePat)
    , currentInstruction(0)
    , alive(true)

{
    Sprite.setPosition(SpawnPos);
}

void Enemy::update(sf::Time frameTime, sf::Vector2u screensize)
{
    if (currentInstruction >= MovePat.size()) 
    {
        alive = false;

        return;
    }

    //Get Target 
    sf::Vector2f Target = MovePat[currentInstruction];
    //Get distance vector from this target point
    sf::Vector2f distanceVector = Target - Sprite.getPosition();
    float magnitude = std::sqrt((distanceVector.x * distanceVector.x) + (distanceVector.y * distanceVector.y));
    sf::Vector2f directionVector = distanceVector / magnitude;

    float distancetoTravel = speed * frameTime.asSeconds();


    sf::Vector2f newposition = Sprite.getPosition() + directionVector * distancetoTravel;
    //Check if we reach the target in the next move
    if (magnitude <= distancetoTravel)
        {
        newposition = Target;

        ++currentInstruction;
        }

    Sprite.setPosition(newposition);

    bulletCooldownRemaining -= frameTime;

    //When cooldowns done the enemy starts blastin
    if (bulletCooldownRemaining <= sf::seconds(0.0f))
    {
        sf::Vector2f bulletPos = Sprite.getPosition();
        bulletPos.y += Sprite.getTexture()->getSize().y / 2 - bulletTexture->getSize().y / 2;
        bulletPos.x += Sprite.getTexture()->getSize().x / 2 - bulletTexture->getSize().x / 2;
        bullets->push_back(Bullet(*bulletTexture, screensize, bulletPos, sf::Vector2f(-1000, 0)));
        bulletCooldownRemaining = bulletCooldownMax;
        bulletfireSound.play();
    }

}

void Enemy::draw(sf::RenderWindow& gameWindow)
{
    gameWindow.draw(Sprite);
}

bool Enemy::getalive()
{
    return alive;
}

sf::FloatRect Enemy::getHitbox()
{
    return sf::FloatRect(Sprite.getGlobalBounds());
}

void Enemy::SetAlive(bool newAlive)
{
    alive = newAlive;
}






