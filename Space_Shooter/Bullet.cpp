#include "Bullet.h"

Bullet::Bullet(sf::Texture& bulletTexture, sf::Vector2u newscreenbounds, sf::Vector2f startpos, sf::Vector2f newVelocity)
	:Sprite(bulletTexture)
	, Velocity(newVelocity)
	, screenBounds(newscreenbounds)
	, Alive(true)

{
	Sprite.setPosition(startpos);
}

void Bullet::update(sf::Time frameTime)
{
	//set the sprites new position to be equal to the current position + the speed
	sf::Vector2f newPosition = Sprite.getPosition() + Velocity * frameTime.asSeconds();

	if (newPosition.x + Sprite.getTexture()->getSize().x < 0 || newPosition.x + Sprite.getTexture()->getSize().x > screenBounds.x) //if the bullet goes off screen delete
	{
		Alive = false;
	}



	Sprite.setPosition(newPosition);
}

void Bullet::draw(sf::RenderWindow& gameWindow)
{
	gameWindow.draw(Sprite);
}

bool Bullet::getalive()
{
	return Alive;
}

sf::FloatRect Bullet::getHit()
{
	return sf::FloatRect(Sprite.getGlobalBounds());
}

void Bullet::SetAlive(bool newAlive)
{
	Alive = newAlive;
}

