#pragma once
#include<SFML/Graphics.hpp>
#include<SFML/Audio.hpp>
#include <vector>
#include "Bullet.h"

class Enemy
{
public:

	//Constructor
	Enemy(sf::Texture& Enemytexture, sf::Vector2u screensize, std::vector<Bullet>& newBullets, sf::Texture& newBulletTexture, sf::SoundBuffer& firingSoundBuffer, sf::Vector2f spawnposition, std::vector< sf::Vector2f > NewMovePat);

	void update(sf::Time frameTime, sf::Vector2u screensize);
	void draw(sf::RenderWindow& gameWindow);
	bool getalive();
	sf::FloatRect getHitbox();

	//setters
	void SetAlive(bool newAlive);
	sf::Sprite Sprite;
private:

	//variables
	sf::Vector2f Velocity;
	
	float speed;
	std::vector<Bullet>* bullets;
	sf::Texture* bulletTexture;
	sf::Time bulletCooldownRemaining;
	sf::Time bulletCooldownMax;
	sf::Sound bulletfireSound;
	std::vector< sf::Vector2f > MovePat;
	int currentInstruction;
	bool alive;
};

