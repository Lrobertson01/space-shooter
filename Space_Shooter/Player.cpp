#include "Player.h"



Player::Player(sf::Texture& playertexture, sf::Vector2u screensize, std::vector<Bullet>& newBullets, sf::Texture& newBulletTexture, sf::SoundBuffer& firingSoundBuffer) //Use an initialisation list
    :Velocity(0.0f, 0.0f)
    , Sprite(playertexture)
    , speed(600.f)
    , bullets(newBullets)
    , bulletTexture(newBulletTexture)
    , bulletCooldownRemaining(sf::seconds(0.0f))
    , bulletCooldownMax(sf::seconds(0.5f))
    , bulletfireSound(firingSoundBuffer)
    , lives()

{
    reset(screensize, playertexture);
}

void Player::reset(sf::Vector2u screensize, sf::Texture& playertexture)
{
    Sprite.setPosition(screensize.x / 2 - playertexture.getSize().x / 2, screensize.y / 2 - playertexture.getSize().y / 2);
    lives = 3;
}

sf::Vector2f Player::GetPos()
{
    
    return Sprite.getPosition();
}

sf::FloatRect Player::getHitbox()
{
    return sf::FloatRect(Sprite.getGlobalBounds());
}

void Player::setLives(bool newlives)
{
    lives -= newlives;
}

int Player::getLives()
{
    return lives;
}
 

void Player::input(sf::Vector2u screensize)
{
    //Reset velocity at the start of each frame
    Velocity.x = 0.0f;

    Velocity.y = 0.0f;

    //movement
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
    {
        // left key is pressed: move our character
        Velocity.x = speed;
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
    {
        // left key is pressed: move our character
        Velocity.x = -speed;
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
    {
        // left key is pressed: move our character
        Velocity.y = speed;
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
    {
        // left key is pressed: move our character
        Velocity.y = -speed;
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && bulletCooldownRemaining <= sf::seconds(0.0f))
    {
        sf::Vector2f bulletPos = Sprite.getPosition();
        bulletPos.y += Sprite.getTexture()->getSize().y / 2 -bulletTexture.getSize().y / 2;
        bulletPos.x += Sprite.getTexture()->getSize().x / 2 - bulletTexture.getSize().x / 2;
        bullets.push_back(Bullet(bulletTexture, screensize, bulletPos, sf::Vector2f(1000, 0)));
        bulletCooldownRemaining = bulletCooldownMax;
        bulletfireSound.play();
    }
}

void Player::update(sf::Time frameTime, sf::Vector2u screensize)
{
    //set the character to move when they have a velocity
    Sprite.setPosition(Sprite.getPosition() + Velocity * frameTime.asSeconds());

    //TODO it collides properly with the left and up, but not right and down. Figure out how to make that work
    if (Sprite.getPosition().x <= 0)
    {
        Sprite.setPosition(0, Sprite.getPosition().y);
    }

    if (Sprite.getPosition().x >= screensize.x)
    {
        Sprite.setPosition(screensize.x, Sprite.getPosition().y - screensize.y);
    }

    if (Sprite.getPosition().y <= 0)
    {
        Sprite.setPosition(Sprite.getPosition().x, 0) ;
    }

    if (Sprite.getPosition().y >= screensize.y)
    {
        Sprite.setPosition(Sprite.getPosition().x, screensize.y);
    }

    bulletCooldownRemaining -= frameTime;


}

void Player::draw(sf::RenderWindow& gameWindow)
{
    gameWindow.draw(Sprite);
}

