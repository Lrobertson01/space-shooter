#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <vector>
#include "Bullet.h"

class Player
{
public:

	//Constructor
	Player(sf::Texture& playertexture, sf::Vector2u screensize, std::vector<Bullet>& newBullets, sf::Texture&newBulletTexture, sf::SoundBuffer& firingSoundBuffer);

	void input(sf::Vector2u screensize);
	void update(sf::Time frameTime, sf::Vector2u screensize);
	void draw(sf::RenderWindow &gameWindow);
	void reset(sf::Vector2u screensize, sf::Texture& playertexture);
	sf::Vector2f GetPos();
	sf::FloatRect getHitbox();
	void setLives(bool newLives);
	int getLives();
	


private:

	//variables
	sf::Vector2f Velocity;
	sf::Sprite Sprite;
	float speed;
	std::vector<Bullet> &bullets;
	sf::Texture& bulletTexture;
	sf::Time bulletCooldownRemaining;
	sf::Time bulletCooldownMax;
	sf::Sound bulletfireSound;
	int lives;

};

